import {
  GET_SINGLE_USER,
  GET_USER_AVAILABLE_SKILL,
  ADD_SKILL,
  GET_ENDORSED_SKILLS,
  ENDORSE_SKILL
} from '../actions/userTypes';
import axios from 'axios';
import { authHeader } from '../helpers/auth-header';

export const getSingleUser = username => async dispatch => {
  const res = await axios.get(
    `${process.env.REACT_APP_API_URL}/users/${username}`,
    {
      headers: authHeader()
    }
  );
  dispatch({
    type: GET_SINGLE_USER,
    payload: res.data
  });
};

export const getUserAvailableSkills = username => async dispatch => {
  const res = await axios.get(
    `${process.env.REACT_APP_API_URL}/skills/available`,
    {
      // params: { username: username },
      headers: authHeader()
    }
  );
  dispatch({
    type: GET_USER_AVAILABLE_SKILL,
    payload: res.data
  });
};

export const addSkill = (username, skill) => async dispatch => {
  const res = await axios.post(
    `${process.env.REACT_APP_API_URL}/users/${username}/skill`,
    skill,
    {
      headers: authHeader()
    }
  );
  dispatch({
    type: ADD_SKILL,
    payload: res.data
  });
};

export const getEndorsedSkills = username => async dispatch => {
  let res = await axios.get(`${process.env.REACT_APP_API_URL}/user`, {
    headers: authHeader()
  });
  const user = res.data;
  res = await axios.get(
    `${process.env.REACT_APP_API_URL}/users/${username}/skill/endorsed`,
    { headers: { ...authHeader(), endorser: user.id } }
  );
  dispatch({
    type: GET_ENDORSED_SKILLS,
    payload: res.data
  });
};

export const endorseSkill = (endorsedUsername, skillName) => async dispatch => {
  const skill = {
    name: skillName
  };
  const res = await axios.post(
    `${process.env.REACT_APP_API_URL}/users/${endorsedUsername}/skill/endorse`,
    skill,
    { headers: authHeader() }
  );
  dispatch({
    type: ENDORSE_SKILL,
    payload: res.data
  });
};
