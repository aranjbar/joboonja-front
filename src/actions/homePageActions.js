import { GET_USER_LIST } from './userTypes';
import { GET_PROJECT_LIST } from './projectTypes';
import axios from 'axios';
import { authHeader } from '../helpers/auth-header';

export const getProjectList = (itemsPerPage, pageNumber) => async dispatch => {
  const res = await axios.get(`${process.env.REACT_APP_API_URL}/projects`, {
    headers: authHeader(),
    params: {
      itemsPerPage,
      pageNumber
    }
  });
  dispatch({
    type: GET_PROJECT_LIST,
    payload: res.data
  });
};

export const getUserList = () => async dispatch => {
  const res = await axios.get(`${process.env.REACT_APP_API_URL}/users`, {
    headers: authHeader()
  });
  dispatch({
    type: GET_USER_LIST,
    payload: res.data
  });
};
