import { GET_SINGLE_PROJECT, BID_PROJECT } from './projectTypes';
import axios from 'axios';
import { authHeader } from '../helpers/auth-header';

export const getSingleProject = id => async dispatch => {
  const res = await axios.get(
    `${process.env.REACT_APP_API_URL}/projects/${id}`,
    { headers: authHeader() }
  );
  dispatch({
    type: GET_SINGLE_PROJECT,
    payload: res.data
  });
};

export const bidProject = (id, bid) => async dispatch => {
  const res = await axios.post(
    `${process.env.REACT_APP_API_URL}/projects/${id}/bid`,
    bid,
    { headers: authHeader() }
  );
  dispatch({
    type: BID_PROJECT,
    payload: res.data
  });
};
