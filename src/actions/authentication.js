import axios from 'axios';
import { SET_LOGGEDIN_USER } from './userTypes';
import { LOGOUT, LOGIN } from './headerTypes';
import { authHeader } from '../helpers/auth-header';

export const login = user => async dispatch => {
  const { username, password } = user;
  const userPass = JSON.stringify({ username, password });
  const res = await axios.post(
    `${process.env.REACT_APP_API_URL}/users/authenticate`,
    userPass,
    {
      headers: {
        'Content-Type': 'application/json'
      }
    }
  );
  if (res.data) {
    localStorage.setItem('token', res.data);
  }

  dispatch({
    type: LOGIN,
    payload: res.data
  });
};

export const logout = () => dispatch => {
  localStorage.removeItem('token');
  dispatch({
    type: LOGOUT,
    payload: {}
  });
};

export const isValidToken = async token => {
  const res = await axios.get(`${process.env.REACT_APP_API_URL}/verifytoken`, {
    headers: authHeader()
  });
  if (res.status === 200) {
    return true;
  } else {
    return false;
  }
};

export const getLoggedInUser = () => async dispatch => {
  const res = await axios.get(`${process.env.REACT_APP_API_URL}/user`, {
    headers: authHeader()
  });
  dispatch({
    type: SET_LOGGEDIN_USER,
    payload: res.data
  });
};

export const signup = user => async dispatch => {
  const res = await axios.post(
    `${process.env.REACT_APP_API_URL}/users/register`,
    user
  );
  console.log(res);
};
