import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import PrivateRoute from './components/login/PrivateRoute';
import Header from './components/layout/Header';
import Footer from './components/layout/Footer';
import UserProfile from './components/users/UserProfile';
import LoginPage from './components/login/LoginPage';
import HomePage from './components/home-page/HomePage';
import ProjectPage from './components/project-page/ProjectPage';
import NotFound from './components/NotFound';

import { Provider } from 'react-redux';
import store from './store';

import './assets/fonts/iransans-fonts/fonts.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/App.scss';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div className="App">
            <div className="content">
              <Header branding="joboonja" />
              <Switch>
                <PrivateRoute exact path="/" component={HomePage} />
                <PrivateRoute
                  exact
                  path="/users/:username"
                  component={UserProfile}
                />
                <PrivateRoute
                  exact
                  path="/project/:id"
                  component={ProjectPage}
                />
                <Route exact path="/login" component={LoginPage} />
                <Route component={NotFound} />
              </Switch>
            </div>
            <Footer />
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
