import { LOGOUT, LOGIN } from '../actions/headerTypes';
import { SET_LOGGEDIN_USER } from '../actions/userTypes';

const initialState = {
  isLoggedIn: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case LOGOUT:
      return {
        ...state,
        isLoggedIn: false
      };
    case LOGIN:
      return {
        ...state,
        isLoggedIn: true
      };
    case SET_LOGGEDIN_USER:
      return {
        ...state,
        isLoggedIn: true
      };
    default:
      return state;
  }
}
