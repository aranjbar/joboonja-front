import { combineReducers } from 'redux';
import headerReducer from './headerReducer';
import userReducer from './userReducer';
import projectReducer from './projectReducer';

export default combineReducers({
  header: headerReducer,
  user: userReducer,
  project: projectReducer
});
