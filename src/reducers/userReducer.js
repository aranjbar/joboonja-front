import {
  GET_SINGLE_USER,
  GET_USER_AVAILABLE_SKILL,
  ADD_SKILL,
  GET_ENDORSED_SKILLS,
  SET_LOGGEDIN_USER,
  ENDORSE_SKILL,
  GET_USER_LIST
} from '../actions/userTypes';

import { LOGOUT } from '../actions/headerTypes';

const initialState = {
  userList: [],
  profile: {
    skills: []
  },
  availableSkills: [],
  endorsedSkills: [],
  loggedInUser: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_SINGLE_USER:
      return {
        ...state,
        profile: action.payload
      };
    case GET_USER_AVAILABLE_SKILL:
      return {
        ...state,
        availableSkills: action.payload
      };
    case ADD_SKILL:
      return {
        ...state,
        profile: {
          ...state.profile,
          skills: [...state.profile.skills, action.payload]
        }
      };
    case GET_ENDORSED_SKILLS:
      return {
        ...state,
        endorsedSkills: action.payload
      };
    case SET_LOGGEDIN_USER:
      return {
        ...state,
        loggedInUser: action.payload
      };
    case ENDORSE_SKILL:
      return {
        ...state,
        profile: {
          ...state.profile,
          skills: state.profile.skills.map(skill =>
            skill.name === action.payload.name ? action.payload : skill
          )
        },
        endorsedSkills: [...state.endorsedSkills, action.payload.name]
      };
    case GET_USER_LIST:
      return {
        ...state,
        userList: action.payload
      };
    case LOGOUT:
      return {
        ...state,
        loggedInUser: {}
      };
    default:
      return state;
  }
}
