import {
  GET_PROJECT_LIST,
  GET_SINGLE_PROJECT,
  BID_PROJECT
} from '../actions/projectTypes';

const initialState = {
  projectList: [],
  projectInfo: {
    skills: [],
    budget: 0,
    deadline: 0
  }
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_PROJECT_LIST:
      return {
        ...state,
        projectList: action.payload
      };
    case GET_SINGLE_PROJECT:
      return {
        ...state,
        projectInfo: action.payload
      };
    case BID_PROJECT:
      return {
        ...state
      };
    default:
      return state;
  }
}
