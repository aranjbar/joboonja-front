export const persianNumber = number => {
  const persianNums = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
  return number
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
    .split('')
    .map(c => ('0123456789'.includes(c) ? persianNums[c] : c))
    .join('');
};
