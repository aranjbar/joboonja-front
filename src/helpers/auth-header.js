export const authHeader = () => {
  // return authorization header with basic auth credentials.
  const token = localStorage.getItem('token');

  if (token) {
    return { Authorization: 'Bearer ' + token };
  } else {
    return {};
  }
};
