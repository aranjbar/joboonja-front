import React from 'react';

const Slideshow = () => {
  return (
    <div id="slideshow">
      <div className="slide-wrapper">
        <div className="slide">
        </div>
        <div className="slide">
        </div>
        <div className="slide">
        </div>
        <div className="slide">
        </div>
        <div className="slide">
        </div>
      </div>
    </div>
  );
};

export default Slideshow;
