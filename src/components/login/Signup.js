import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TextInputGroup from '../layout/TextInputGroup';
import { signup } from '../../actions/authentication';
import { connect } from 'react-redux';

class Signup extends Component {
  state = {
    firstName: '',
    lastName: '',
    id: '',
    password: '',
    jobTitle: '',
    imageUrl: '',
    bio: '',
    errors: {}
  };

  onSubmit = e => {
    e.preventDefault();

    const {
      firstName,
      lastName,
      id,
      password,
      jobTitle,
      imageUrl,
      bio,
      errors
    } = this.state;

    // Check for errors
    const format = /[ !@#$%^&*()+\-=\[\]{};':"\\|,.<>\/?]/;
    if (id === '') {
      this.setState({ errors: { id: 'username is required.' } });
      return;
    } else if (format.test(id)) {
      this.setState({
        errors: { id: 'Only a-z, 0-9, and underscors allowed.' }
      });
      return;
    } else if (id.length < 4) {
      this.setState({ errors: { id: 'This username is too short.' } });
      return;
    }

    if (password === '') {
      this.setState({ errors: { password: 'Password is required.' } });
      return;
    } else if (password.length < 8) {
      this.setState({ errors: { password: 'Use 8 or more characters.' } });
      return;
    }

    if (
      firstName === '' ||
      lastName === '' ||
      jobTitle === '' ||
      imageUrl === '' ||
      bio === ''
    ) {
      this.setState({ errors: { requiredField: 'This field is required.' } });
    }

    const user = {
      firstName,
      lastName,
      id,
      password,
      jobTitle,
      imageUrl,
      bio
    };

    this.props.signup(user);

    // Clear State
    this.setState({
      firstName: '',
      lastName: '',
      id: '',
      password: '',
      jobTitle: '',
      imageUrl: '',
      bio: '',
      errors: {}
    });
  };
  onChange = e => this.setState({ [e.target.name]: e.target.value.trim() });

  render() {
    const {
      firstName,
      lastName,
      id,
      password,
      jobTitle,
      imageUrl,
      bio,
      errors
    } = this.state;
    return (
      <div className="col-lg-5 col-xs-12 signup-container">
        ثبت‌نام
        <form onSubmit={this.onSubmit}>
          <TextInputGroup
            name="firstName"
            placeholder="نام"
            value={firstName}
            onChange={this.onChange}
            error={errors.requiredField}
          />
          <br />
          <TextInputGroup
            name="lastName"
            placeholder="نام‌خانوادگی‌"
            value={lastName}
            onChange={this.onChange}
            error={errors.requiredField}
          />
          <br />
          <TextInputGroup
            name="id"
            placeholder="نام‌کاربری"
            value={id}
            onChange={this.onChange}
            error={errors.id}
          />
          <br />
          <TextInputGroup
            name="password"
            type="password"
            placeholder="رمزعبور"
            value={password}
            onChange={this.onChange}
            error={errors.password}
          />
          <br />
          <TextInputGroup
            name="jobTitle"
            placeholder="عنوان شغلی"
            value={jobTitle}
            onChange={this.onChange}
            error={errors.requiredField}
          />
          <br />
          <TextInputGroup
            name="imageUrl"
            placeholder="لینک عکس پروفایل"
            value={imageUrl}
            onChange={this.onChange}
            error={errors.requiredField}
          />
          <br />
          <TextInputGroup
            name="bio"
            placeholder="بیو"
            value={bio}
            onChange={this.onChange}
            error={errors.requiredField}
          />
          <br />
          <input
            type="submit"
            value="ثبت‌نام کن"
            className="btn btn-success btn-block"
          />
        </form>
      </div>
    );
  }
}

Signup.propTypes = {
  signup: PropTypes.func.isRequired
};

export default connect(
  null,
  { signup }
)(Signup);
