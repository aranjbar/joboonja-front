import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { isValidToken } from '../../actions/authentication';

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      localStorage.getItem(
        'token'
      ) /*&&
      isValidToken(localStorage.getItem('token'))*/ ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{ pathname: '/login', state: { from: props.location } }}
        />
      )
    }
  />
);

export default PrivateRoute;
