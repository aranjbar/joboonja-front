import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import TextInputGroup from '../layout/TextInputGroup';
import { login } from '../../actions/authentication';
import { connect } from 'react-redux';

class Login extends Component {
  state = {
    username: '',
    password: '',
    errors: {}
  };

  onSubmit = async e => {
    e.preventDefault();

    const { username, password } = this.state;
    const { from } = this.props;
    // console.log(from);
    // Check for errors
    if (username === '') {
      this.setState({ errors: { username: 'Username is required.' } });
      return;
    }

    if (password === '') {
      this.setState({ errors: { password: 'Password is required.' } });
      return;
    }

    const user = {
      username,
      password
    };

    await this.props.login(user);

    // Clear State
    this.setState({
      username: '',
      password: '',
      errors: {}
    });

    this.props.history.push(from);
  };

  onChange = e => this.setState({ [e.target.name]: e.target.value });

  render() {
    const { username, password, errors } = this.state;
    return (
      <div className="col-lg-5 col-xs-12 login-container">
        ورود
        <form onSubmit={this.onSubmit}>
          <TextInputGroup
            name="username"
            placeholder="نام‌کاربری"
            value={username}
            onChange={this.onChange}
            error={errors.username}
          />
          <br />
          <TextInputGroup
            name="password"
            type="password"
            placeholder="رمزعبور"
            value={password}
            onChange={this.onChange}
            error={errors.password}
          />
          <br />
          <input
            type="submit"
            value="وارد شو"
            className="btn btn-success btn-block theSubmitButton"
          />
        </form>
      </div>
    );
  }
}

Login.propTypes = {
  login: PropTypes.func.isRequired
};

export default withRouter(
  connect(
    null,
    { login }
  )(Login)
);
