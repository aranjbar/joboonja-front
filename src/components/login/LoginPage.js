import React, { Component } from 'react';
import Slideshow from './Slideshow';
import Login from './Login';
import Signup from './Signup';

import '../../styles/LoginPage.scss';

export default class LoginPage extends Component {
  render() {
    const { from } = this.props.location.state || { from: { pathname: '/' } };
    return (
      <div>
        <Slideshow />
        <div className="login-signup-container row">
          <Login from={from} />

          <div className="col-lg-2">
            <div className="divider" />
          </div>

          <Signup from={from} />
        </div>
      </div>
    );
  }
}
