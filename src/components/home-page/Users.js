import React, { Component } from 'react';
import UserSearchBar from './UserSearchBar';
import UserBrief from './UserBrief';
import { connect } from 'react-redux';
import { getUserList } from '../../actions/homePageActions';

class Users extends Component {
  componentDidMount() {
    this.props.getUserList();
  }
  render() {
    const { users } = this.props;
    return (
      <div className="col-lg-3 col-xs-4 col-12">
        <UserSearchBar />
        {users.map(user => (
          <UserBrief key={user.id} user={user} />
        ))}
      </div>
    );
  }
}

const mapStateToProps = state => ({ users: state.user.userList });

export default connect(
  mapStateToProps,
  { getUserList }
)(Users);
