import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Project from './Project';
import { getProjectList } from '../../actions/homePageActions';
import { persianNumber } from '../../helpers/persianNumbers';
import { connect } from 'react-redux';
import uuid from 'uuid';
import classnames from 'classnames';

class Projects extends Component {
  state = {
    pageNumber: 1,
    itemsPerPage: 18,
    itemsNumber: 18,
    error: ''
  };
  componentDidMount() {
    const { itemsPerPage, pageNumber } = this.state;
    this.props.getProjectList(itemsPerPage, pageNumber);
  }

  onSubmit = e => {
    e.preventDefault();
    const { itemsPerPage, pageNumber } = this.state;
    console.log(itemsPerPage, pageNumber);
    this.props.getProjectList(itemsPerPage, pageNumber);
  };

  render() {
    const { itemsPerPage, pageNumber, itemsNumber, error } = this.state;
    const { projectList } = this.props;

    return (
      <div className="col-lg-9 col-xs-8 col-12">
        {projectList.map(project => (
          <Project key={uuid()} project={project} />
        ))}
        <div className="pager" dir="ltr">
          <div className="item-per-page input-group">
            <form onSubmit={this.onSubmit} className="input-group">
              <input
                className="btn btn-primary"
                type="submit"
                value="انتخاب"
                disabled={itemsNumber === ''}
              />
              <input
                className={classnames({ 'is-invalid': error })}
                type="number"
                name="itemsPerPage"
                value={itemsPerPage}
                onChange={e => {
                  this.setState({ [e.target.name]: e.target.value });
                }}
                placeholder="تعداد در صفحه"
              />
            </form>
          </div>
          {pageNumber !== 1 && (
            <i
              className="fas fa-angle-left fa-lg"
              style={{ cursor: 'pointer', float: 'right' }}
              onClick={() => {
                if (pageNumber !== 1) {
                  this.setState({ pageNumber: pageNumber - 1 });
                  this.props.getProjectList(itemsPerPage, pageNumber - 1);
                  window.scrollTo(0, 0);
                }
              }}
            />
          )}

          <div className="page">{persianNumber(pageNumber)}</div>
          {projectList.length >= itemsPerPage && (
            <i
              className="fas fa-angle-right fa-lg"
              style={{ cursor: 'pointer', float: 'right' }}
              onClick={() => {
                this.setState({ pageNumber: pageNumber + 1 });
                this.props.getProjectList(itemsPerPage, pageNumber + 1);
                window.scrollTo(0, 0);
              }}
            />
          )}
        </div>
      </div>
    );
  }
}

Projects.propTypes = {
  projectList: PropTypes.array.isRequired,
  getProjectList: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  projectList: state.project.projectList
});

export default connect(
  mapStateToProps,
  { getProjectList }
)(Projects);
