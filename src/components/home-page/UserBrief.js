import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import profileImagePlaceholder from '../../assets/images/placeholder-face-big.png';

class UserBrief extends Component {
  placeholder = e => {
    e.target.src = profileImagePlaceholder;
  };
  render() {
    const { id, firstName, jobTitle, imageUrl } = this.props.user;
    return (
      <div className="user-card" dir="rtl">
        <div className="row">
          <div className="col-lg-3 col-2" style={{ textAlign: 'right' }}>
            <Link to={`/users/${id}`}>
              <img
                src={imageUrl || profileImagePlaceholder}
                alt={id}
                onError={this.placeholder}
              />
            </Link>
          </div>
          <div className="col-lg-9 col-10" style={{ paddingRight: 15 }}>
            <Link to={`/users/${id}`} className="user-brief-title">
              <p className="user-name">{firstName}</p>
            </Link>
            <p className="user-bio">{jobTitle}</p>
          </div>
        </div>
      </div>
    );
  }
}

UserBrief.propTypes = {
  user: PropTypes.object.isRequired
};

export default UserBrief;
