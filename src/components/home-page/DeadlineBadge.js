import React, { Component } from 'react';
import moment from 'moment';
import classnames from 'classnames';
import { persianNumber } from '../../helpers/persianNumbers';

class DeadlineBadge extends Component {
  render() {
    const { deadline } = this.props;
    const ms = moment(deadline, 'x').diff(moment());
    const d = moment.duration(ms);
    const days = Math.floor(d.asDays());
    const s =
      days > 0
        ? days + ' روز ،' + moment.utc(ms).format(' hh:mm:ss')
        : moment.utc(ms).format('hh:mm:ss');
    return (
      <span
        className={classnames('badge', 'badge-light', {
          'badge-green': d.valueOf() < 0
        })}
      >
        {d.valueOf() > 0
          ? 'زمان باقی‌مانده: ' + persianNumber(s)
          : 'مهلت تمام شده'}
      </span>
    );
  }
}

export default DeadlineBadge;
