import React, { Component } from 'react';
import BlueStrip from '../layout/BlueStrip';
import IntroSearch from './IntroSearch';
import Projects from './Projects';
import Users from './Users';

import '../../styles/HomePage.scss';

class HomePage extends Component {
  render() {
    return (
      <div>
        <BlueStrip height={300} />
        <div className="container">
          <IntroSearch />

          <section className="row projects-list">
            {/* <!-- Projects list --> */}
            <Projects />

            {/* <!-- Freelancers list --> */}
            <Users />
          </section>
        </div>
      </div>
    );
  }
}

export default HomePage;
