import React, { Component } from 'react';

class IntroSearch extends Component {
  render() {
    return (
      <section className="intro-search-section">
        <div className="row" dir="rtl">
          <h1>جاب‌اونجا خوب است!</h1>
        </div>
        <div className="row" dir="rtl">
          <p>
            اگر می‌خواهید که پول دربیارید، لطفا فریلنسر نشوید چونکه دهنتون سرویس
            میشه. من تجربه‌ش رو دارم. لطفا این سایت رو ترک کنید! همین الان!!!
          </p>
        </div>
        <div className="row">
          <div className="col-lg-6 offset-lg-3 col-8 offset-2 search-bar">
            <div className="input-group" dir="rtl">
              <input type="text" placeholder="جستجو در جاب‌اونجا" />
              <span>
                <button type="submit">جستجو</button>
              </span>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default IntroSearch;
