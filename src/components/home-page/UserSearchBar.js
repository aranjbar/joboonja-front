import React from 'react';

const UserSearchBar = () => {
  return (
    <div className="user-search-bar">
      <input type="text" placeholder="جستجوی نام کاربر" />
    </div>
  );
};

export default UserSearchBar;
