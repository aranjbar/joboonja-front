import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import DeadlineBadge from './DeadlineBadge';
import SkillBadge from './SkillBadge';
import { persianNumber } from '../../helpers/persianNumbers';
import projectPlaceholder from '../../assets/images/project-placeholder.png';
import uuid from 'uuid';

class Project extends Component {
  placeholder = e => {
    e.target.src = projectPlaceholder;
  };
  render() {
    const {
      title,
      skills,
      budget,
      id,
      description,
      imageUrl,
      deadline
    } = this.props.project;
    return (
      <div className="row project-card" dir="rtl">
        <div className="col-lg-3 col-3">
          <img src={imageUrl} alt={id} onError={this.placeholder} />
        </div>
        <div className="col-lg-9 col-9 project-info">
          <div className="row" dir="rtl">
            <div
              className="col-lg-8 col-sm-6"
              style={{ paddingLeft: 0 }}
              dir="rtl"
            >
              <Link to={`/project/${id}`} className="project-title-link">
                <h6>{title}</h6>
              </Link>
            </div>
            <div className="col-lg-4 col-sm-6 project-deadline" dir="ltr">
              <DeadlineBadge deadline={deadline} />
            </div>
          </div>
          <p>{description}</p>
          <p className="project-budget">بودجه: {persianNumber(budget)} تومان</p>
          <p className="project-required-skills">
            {skills && <span>مهارت‌ها:</span> &&
              skills.map(skill => (
                <SkillBadge key={uuid()} name={skill.name} />
              ))}
          </p>
        </div>
      </div>
    );
  }
}

Project.propTypes = {
  project: PropTypes.object.isRequired
};

export default Project;
