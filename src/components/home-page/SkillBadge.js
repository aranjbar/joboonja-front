import React, { Component } from 'react';
import PropTypes from 'prop-types';

class SkillBadge extends Component {
  render() {
    const { name } = this.props;
    return <span className="badge badge-light">{name}</span>;
  }
}

SkillBadge.propTypes = {
  name: PropTypes.string.isRequired
};

export default SkillBadge;
