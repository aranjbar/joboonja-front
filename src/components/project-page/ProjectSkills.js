import React, { Component } from 'react';
import Skill from '../users/Skill';
import uuid from 'uuid';

export default class ProjectSkills extends Component {
  render() {
    const { skills } = this.props;
    return (
      <div className="row project-skills">
        <div className="col-12">
          <div className="row" dir="rtl">
            <h6>مهارت‌های لازم:</h6>
          </div>
          <div className="row">
            <div className="row user-skills" dir="ltr">
              {skills.map(skill => (
                <Skill
                  key={uuid()}
                  skill={skill}
                  loggedIn={true}
                  endorsed={[]}
                  username={'1'}
                />
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
