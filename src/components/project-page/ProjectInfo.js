import React, { Component } from 'react';
import projectPlaceholder from '../../assets/images/project-placeholder.png';
import { persianNumber } from '../../helpers/persianNumbers';
import BidRemainingTime from './BidRemainingTime';

export default class ProjectInfo extends Component {
  render() {
    const { imageUrl, title, deadline, budget, id, description } = this.props;
    return (
      <div className="row project-info" dir="rtl">
        <div className="col-lg-2 col-md-4 project-image-container">
          <img src={imageUrl || projectPlaceholder} alt={id} />
        </div>
        <div className="col-lg-9 col-md-7" dir="rtl">
          <h1>{title}</h1>

          <ul className="project-quick-info">
            <li className="bid-remaining-time">
              <i className="flaticon-deadline" />
              <BidRemainingTime deadline={deadline} />
            </li>
            <li className="project-budget">
              <span>
                <i className="flaticon-money-bag" />
                بودجه: {persianNumber(budget)} تومان
              </span>
            </li>
          </ul>

          <h4>توضیحات</h4>
          <p>{description}</p>
        </div>
      </div>
    );
  }
}
