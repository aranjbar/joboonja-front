import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getSingleProject, bidProject } from '../../actions/projectPageActions';
import BlueStrip from '../layout/BlueStrip';
import ProjectInfo from './ProjectInfo';
import ProjectSkills from './ProjectSkills';
import ProjectBid from './ProjectBid';

import '../../styles/ProjectPage.scss';
import '../../styles/theme.scss';

class ProjectPage extends Component {
  state = {
    bidAmount: '',
    error: ''
  };
  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.getSingleProject(id);
  }
  onSubmit = async e => {
    e.preventDefault();

    const { bidAmount } = this.state;
    const { id } = this.props.match.params;

    if (bidAmount === '') {
      this.setState({ error: 'bid amount is required.' });

      return;
    }

    const bid = {
      bidAmount: bidAmount
    };
    await this.props.bidProject(id, bid);

    this.setState({
      bidAmount: '',
      error: ''
    });
  };
  onChange = e => this.setState({ bidAmount: e.target.value });

  render() {
    const { id } = this.props.match.params;
    const {
      title,
      skills,
      budget,
      description,
      deadline,
      imageUrl
    } = this.props.project;
    const { bidAmount, error } = this.state;
    return (
      <div>
        <BlueStrip height={120} />
        <div className="container card content-on-blue-strip project-bid-container ">
          <ProjectInfo
            imageUrl={imageUrl}
            title={title}
            deadline={deadline}
            budget={budget}
            id={id}
            description={description}
          />

          <ProjectSkills skills={skills} />
          <ProjectBid
            onChange={this.onChange}
            onSubmit={this.onSubmit}
            bidAmount={bidAmount}
            error={error}
            deadline={deadline}
          />
        </div>
      </div>
    );
  }
}

ProjectPage.propTypes = {
  project: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  project: state.project.projectInfo
});

export default connect(
  mapStateToProps,
  { getSingleProject, bidProject }
)(ProjectPage);
