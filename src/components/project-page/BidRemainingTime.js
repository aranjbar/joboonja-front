import React, { Component } from 'react';
import moment from 'moment';
import { persianNumber } from '../../helpers/persianNumbers';

export default class BidRemainingTime extends Component {
  render() {
    const { deadline } = this.props;
    let ms = moment(deadline, 'x').diff(moment());
    let d = moment.duration(ms);
    if (d.valueOf() < 0) {
      return <span className="text-danger">مهلت تمام شده</span>;
    }
    const days = Math.floor(d.asDays());
    ms = ms - days * 86400000;
    d = moment.duration(ms);
    const hours = Math.floor(d.asHours());
    ms = ms - hours * 3600000;
    d = moment.duration(ms);
    const minutes = Math.floor(d.asMinutes());
    ms = ms - minutes * 60000;
    d = moment.duration(ms);
    const seconds = Math.floor(d.asSeconds());
    return (
      <div>
        <span>زمان باقی‌مانده:</span>
        <span>
          {days > 0 && `${persianNumber(days)} روز و `}{' '}
          {hours > 0 && `${persianNumber(hours)} ساعت و `}{' '}
          {minutes > 0 && `${persianNumber(minutes)} دقیقه و `}{' '}
          {`${persianNumber(seconds)} ثانیه`}
        </span>
      </div>
    );
  }
}
