import React, { Component } from 'react';
import classnames from 'classnames';
import moment from 'moment';

export default class ProjectBid extends Component {
  render() {
    const { bidAmount, error, onChange, onSubmit } = this.props;
    const { deadline } = this.props;
    const ms = moment(deadline, 'x').diff(moment());
    const d = moment.duration(ms);
    return (
      <div className="row" dir="rtl">
        <div className="col-lg-6 col-12 project-bid">
          {d.valueOf() > 0 ? (
            <div className="input-group">
              <form className="form-inline" onSubmit={onSubmit}>
                <div>
                  <div className="bid-input-container">
                    <input
                      className={classnames({ 'is-invalid': error })}
                      type="text"
                      id="bid-input"
                      name="bidAmound"
                      value={bidAmount}
                      onChange={onChange}
                      placeholder="پیشنهاد خود را وارد کنید"
                    />
                    <span>تومان</span>
                  </div>
                  {error !== '' && (
                    <div className="invalid-feedback">{error}</div>
                  )}
                </div>
                <div>
                  <span className="input-group-btn">
                    <input
                      id="send"
                      className="btn btn-primary"
                      value="ارسال"
                      type="submit"
                    />
                  </span>
                </div>
              </form>
            </div>
          ) : (
            <div className="text-danger">مهلت تمام شده</div>
          )}
        </div>
      </div>
    );
  }
}
