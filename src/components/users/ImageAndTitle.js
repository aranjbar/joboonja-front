import React, { Component } from 'react';
import PropTypes from 'prop-types';
import profileImagePlaceholder from '../../assets/images/placeholder-face-big.png';

class ImageAndTitle extends Component {
  render() {
    const { fullName, title, imageUrl } = this.props;
    return (
      <div className="row user-image-n-title">
        <div className="user-image-container col-12">
          <img
            src={imageUrl || profileImagePlaceholder}
            alt="user profile"
            className="user-profile-image"
          />
          <div className="the-trapezius-shape the-trapezius-1" />
          <div className="the-trapezius-shape the-trapezius-2" />
          <div className="user-title-container">
            <h1>{fullName}</h1>
            <h4>{title}</h4>
          </div>
        </div>
      </div>
    );
  }
}

ImageAndTitle.propTypes = {
  fullName: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  imageUrl: PropTypes.string.isRequired
};

ImageAndTitle.defaultProps = {
  fullName: '',
  title: '',
  imageUrl: profileImagePlaceholder
};

export default ImageAndTitle;
