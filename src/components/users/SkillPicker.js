import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  getUserAvailableSkills,
  addSkill
} from '../../actions/userProfileActions';
import uuid from 'uuid';

class SkillPicker extends Component {
  state = {
    username: '',
    skillName: '',
    errors: {}
  };
  componentDidMount() {
    const { username } = this.props;
    this.setState({ username: username });
    this.props.getUserAvailableSkills(username);
  }
  onSubmit = e => {
    e.preventDefault();

    const { skillName, username } = this.state;

    // Check For Errors
    if (skillName === '') {
      this.setState({ errors: { skill: 'Select a skill' } });
      return;
    }

    const newSkill = {
      name: skillName
    };
    this.props.addSkill(username, newSkill);
    this.setState({ skillName: '' });
  };

  onChange = () => {
    this.setState({ skillName: document.getElementById('skillPicker').value });
  };
  render() {
    const { skills } = this.props;
    const { skillName } = this.state;
    return (
      <div className="skill-picker">
        <div className="skill-picker-label">
          <label htmlFor="skillPicker">مهارت‌ها: </label>
        </div>
        <form onSubmit={this.onSubmit} className="input-group">
          <select
            onChange={this.onChange}
            id="skillPicker"
            className="form-control"
            value={skillName}
          >
            <option value="">--انتخاب مهارت--</option>
            {skills.map(skill => (
              <option key={uuid()} value={skill.name}>
                {skill.name}
              </option>
            ))}
          </select>
          <span className="input-group-btn">
            <input
              className="btn btn-primary"
              type="submit"
              value="انتخاب مهارت"
              disabled={skillName === ''}
            />
          </span>
        </form>
      </div>
    );
  }
}

SkillPicker.propTypes = {
  skills: PropTypes.array.isRequired,
  getUserAvailableSkills: PropTypes.func.isRequired,
  addSkill: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  skills: state.user.availableSkills
});

export default connect(
  mapStateToProps,
  { getUserAvailableSkills, addSkill }
)(SkillPicker);
