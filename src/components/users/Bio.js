import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Bio extends Component {
  render() {
    const { bio } = this.props;
    return (
      <div className="row">
        <p className="user-description">{`${bio}`}</p>
      </div>
    );
  }
}

Bio.propTypes = {
  bio: PropTypes.string.isRequired
};

Bio.defaultProps = {
  bio: ''
};

export default Bio;
