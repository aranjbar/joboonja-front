import React, { Component } from 'react';
import Skill from './Skill';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import uuid from 'uuid';
import { getLoggedInUser } from '../../actions/authentication';
import { getEndorsedSkills } from '../../actions/userProfileActions';

class Skills extends Component {
  componentDidMount() {
    const { username } = this.props;
    this.props.getEndorsedSkills(username);
    this.props.getLoggedInUser();
  }
  render() {
    const { skills, username, endorsedSkills, loggedInUser } = this.props;
    const token = localStorage.getItem('token');
    const loggedIn = token && loggedInUser.id === username;
    return (
      <div className="row user-skills" dir="ltr">
        {skills.map(skill => (
          <Skill
            key={uuid()}
            skill={skill}
            loggedIn={loggedIn}
            endorsed={endorsedSkills.includes(skill.name)}
            username={username}
          />
        ))}
      </div>
    );
  }
}

Skills.propTypes = {
  skills: PropTypes.array.isRequired,
  username: PropTypes.string.isRequired,
  endorsedSkills: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  skills: state.user.profile.skills,
  endorsedSkills: state.user.endorsedSkills,
  loggedInUser: state.user.loggedInUser
});

export default connect(
  mapStateToProps,
  { getEndorsedSkills, getLoggedInUser }
)(Skills);
