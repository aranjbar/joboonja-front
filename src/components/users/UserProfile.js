import React, { Component } from 'react';
import PropTypes from 'prop-types';
import BlueStrip from '../layout/BlueStrip';
import ImageAndTitle from './ImageAndTitle';
import Bio from './Bio';
import Skills from './Skills';
import SkillPicker from './SkillPicker';
import { connect } from 'react-redux';
import { getSingleUser } from '../../actions/userProfileActions';
import { getLoggedInUser } from '../../actions/authentication';

import '../../styles/userProfile.scss';

class UserProfile extends Component {
  componentDidMount() {
    const { username } = this.props.match.params;
    this.props.getSingleUser(username);
    this.props.getLoggedInUser();
  }
  render() {
    const { username } = this.props.match.params;
    const {
      firstName,
      lastName,
      bio,
      jobTitle,
      skills,
      imageUrl
    } = this.props.user;
    const { loggedInUser } = this.props;
    const fullName = `${firstName} ${lastName}`;
    const token = localStorage.getItem('token');
    return (
      <React.Fragment>
        <BlueStrip height={160} />
        <div className="container user-profile-main-container" dir="rtl">
          <div className="row">
            <div className="col-12 col-lg-12 col-md-12 offset-lg-0 offset-md-0">
              <ImageAndTitle
                fullName={fullName}
                title={jobTitle}
                imageUrl={imageUrl}
              />
              <Bio bio={bio} />
              {token && loggedInUser.id === username && (
                <SkillPicker username={username} />
              )}
              <Skills skills={skills} username={username} />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

UserProfile.propTypes = {
  user: PropTypes.object.isRequired,
  getSingleUser: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  user: state.user.profile,
  loggedInUser: state.user.loggedInUser
});

export default connect(
  mapStateToProps,
  { getSingleUser, getLoggedInUser }
)(UserProfile);
