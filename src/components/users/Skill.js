import React, { Component } from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { endorseSkill } from '../../actions/userProfileActions';

class Skill extends Component {
  state = {
    username: '',
    name: '',
    point: -1,
    loggedIn: false,
    endorsed: false
  };
  componentDidMount() {
    const { name, point } = this.props.skill;
    const { loggedIn, endorsed, username } = this.props;
    this.setState({ username, name, point, loggedIn, endorsed });
  }
  onClick = e => {
    const { username, name } = this.state;
    this.props.endorseSkill(username, name);
  };
  render() {
    const { name, point } = this.props.skill;
    // console.log(this.props.skill);
    const { loggedIn, endorsed } = this.state;
    const zeroEndorsed = point === 0;
    return (
      <div className="user-skill">
        <div className="skill">{name}</div>
        <div
          className={classnames({
            'zero-endorsed': zeroEndorsed,
            endorsed: endorsed || point,
            'for-endorse': !endorsed && !loggedIn
          })}
        >
          {!endorsed && !loggedIn ? (
            <span onClick={this.onClick}>
              <i className="fas fa-plus" />
            </span>
          ) : (
            <span>{point}</span>
          )}
        </div>
      </div>
    );
  }
}

// const mapStateToProps = state => ({
//   skill: state.user.endorsedSkill
// });

export default connect(
  null,
  { endorseSkill }
)(Skill);
