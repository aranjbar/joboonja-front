import React from 'react';

import '../../styles/Footer.scss';

const Footer = () => {
  return (
    <footer className="damned-footer" dir="rtl">
      <span>{'\u00A9'} تمامی حقوق این سایت متعلق به جاب‌اونجا می‌باشد</span>
    </footer>
  );
};

export default Footer;
