import React from 'react';
import PropTypes from 'prop-types';

import '../../styles/BlueStrip.scss';

const BlueStrip = props => {
  const { height } = props;
  return <div className="blue-strip" style={{ height: height }} />;
};

BlueStrip.propTypes = {
  height: PropTypes.number.isRequired
};

BlueStrip.defaultProps = {
  height: 160
};

export default BlueStrip;
