import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { getLoggedInUser, logout } from '../../actions/authentication';
import logo from '../../assets/logo/logo v1.png';

import '../../styles/Header.scss';

class Header extends Component {
  componentDidMount() {
    this.props.getLoggedInUser();
  }
  render() {
    const { branding, isLoggedIn, loggedInUser } = this.props;
    if (isLoggedIn && !loggedInUser.id) {
      this.props.getLoggedInUser();
    }
    return (
      <nav
        className="navbar navbar-expand-lg navbar-expand-sm navbar-light bg-light"
        dir="rtl"
      >
        <div className="container">
          <Link to="/" className="nav-link">
            <div className="navbar-brand-image">
              <img src={logo} alt={branding} />
            </div>
          </Link>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              {isLoggedIn ? (
                <React.Fragment>
                  <li className="nav-item active">
                    <Link
                      to={isLoggedIn ? `/users/${loggedInUser.id}` : ''}
                      className="nav-link"
                    >
                      حساب کاربری
                    </Link>
                  </li>
                  <li className="nav-item active" onClick={this.props.logout}>
                    <Link to="/login" className="nav-link">
                      خروج
                    </Link>
                  </li>
                </React.Fragment>
              ) : (
                <li className="nav-item active">
                  <Link to="/login" className="nav-link">
                    ورود
                  </Link>
                </li>
              )}
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

Header.defaultProps = {
  branding: 'My App'
};

Header.propTypes = {
  branding: PropTypes.string.isRequired,
  getLoggedInUser: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
  isLoggedIn: state.header.isLoggedIn,
  loggedInUser: state.user.loggedInUser
});

export default connect(
  mapStateToProps,
  { getLoggedInUser, logout }
)(Header);
